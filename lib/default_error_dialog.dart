import 'package:flutter/material.dart';

abstract class Dialog {
  static void show() {}

  static Widget someWidget;
}

class DefaultErrorDialog implements Dialog {
  @override
  static Widget someWidget(context) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        shape: BoxShape.circle,
      ),
      margin: EdgeInsets.all(100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Material(
            child: Container(
              height: 100,
              width: 100,
              child: Center(
                child: Image.network('https://klike.net/uploads/posts/2020-04/1587719791_1.jpg', fit: BoxFit.cover,),
              ),
            ),
          ),
          Center(
            child: ElevatedButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text('Закрой меня'),
            ),
          ),
        ],
      ),
    );
  }

  @override
  static void show(context) {
    showDialog(
      context: context,
      builder: (ctx) => someWidget(ctx),
    );
  }
}
