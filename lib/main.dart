import 'package:dialog_helper/dialog_helper.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatelessWidget{
  const MyHomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Dialog Helper'),
      ),
      body: Center(
        child: ElevatedButton(
          onPressed: (){
            DialogHelper.show(context);
          },
          child: Text('Tap me'),
        ),
      ),
    );
  }
}
