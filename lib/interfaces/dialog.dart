import 'package:flutter/material.dart';

abstract class Dialog {
  void show() {}

  Widget widget;
}
